package bl;

public class Autor {

    private String nombre;
    private String pais;
    private String lugarNacimiento;
    private String nacionalidad;
    private int fechaNacimiento;
    private int edad;

    public Autor() {
        this.nombre = "";
        this.pais = "";
        this.lugarNacimiento = "";
        this.nacionalidad = "";
        this.fechaNacimiento = 0;
        this.edad = 0;
    }

    public Autor(String nombre, String pais, String lugarNacimiento,
            String nacionalidad, int fechaNacimiento, int edad) {
        this.nombre = nombre;
        this.pais = pais;
        this.lugarNacimiento = lugarNacimiento;
        this.nacionalidad = nacionalidad;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public int getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(int fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String toString() {
        return "Autor{" + "nombre=" + nombre + ", pais=" + pais
                + ", lugarNacimiento=" + lugarNacimiento + ", nacionalidad="
                + nacionalidad + ", fechaNacimiento=" + fechaNacimiento
                + ", edad=" + edad + '}';
    }

}//fin
