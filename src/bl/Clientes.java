package bl;


public class Clientes {
    private String nombre;
    private String id;
    private String paisCliente;

    public Clientes() {
        this.nombre = "";
        this.id = "";
        this.paisCliente = "";
    }

    public Clientes(String nombre, String id, String paisCliente) {
        this.nombre = nombre;
        this.id = id;
        this.paisCliente = paisCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaisCliente() {
        return paisCliente;
    }

    public void setPaisCliente(String paisCliente) {
        this.paisCliente = paisCliente;
    }

    
    public String toString() {
        return "Clientes{" + "nombre=" + nombre + ", id=" + id 
                + ", paisCliente=" + paisCliente + '}';
    }

   

    
    
    
}//fin
