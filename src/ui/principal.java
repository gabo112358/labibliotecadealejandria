package ui;

import bl.Autor;
import bl.Clientes;
import bl.Libro;
import bl.Categorias;
import java.io.*;

public class principal {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static int fechaActual = 2021;
    static Libro[] listaLibros = new Libro[10];
    static Autor[] listaAutores = new Autor[10];
    static Clientes[] listaClientes = new Clientes[10];
    static Categorias[] listaCategorias = new Categorias[5];
        static int[] listaCodigos = {1, 2, 3, 4, 5};

    public static void main(String[] args) throws IOException {
        menu();
    }

    public static void menu() throws IOException {
        int opcion = 0;

        do {
            System.out.println("***BIENVENIDO A LA LIBRERIA DE ALEJANDRO MAGNO***");
            System.out.println("1)Registro de Categorias");
            System.out.println("2)Registro de Libros ");
            System.out.println("3)Registro de clientes");
            System.out.println("7)Listar categorias");
            System.out.println("4)Listar libros");
            System.out.println("5)Listar autores");
            System.out.println("6)Listar clientes");
            System.out.println("8)Salir");
            System.out.println("Digite la opcion que desee:");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 8);
    }

    public static void procesarOpcion(int opcion) throws IOException {

        switch (opcion) {

            case 1:
                registroCategorias();
                break;
            case 2:
                registroLibros();
                break;
            case 3:
                registroClientes();
                break;
            case 4:
                listarLibros();
                break;
            case 5:
                listarAutor();
                break;
            case 6:
                listarClientes();
                break;
            case 7:
                listarCategorias();
                break;
            case 8:
                System.out.println("Salió de forma correcta");
                System.exit(0);
                break;
            default:
                System.out.println("Opcion no válida");
                break;
        }
    }

    //Registrar Libros
    public static void registroLibros() throws IOException {
        System.out.println("Digite el codigo ISBN");
        int ISBN = Integer.parseInt(in.readLine());
        System.out.println("Digite la fecha de publicacion del libro :");
        int fechaPublicacion = Integer.parseInt(in.readLine());
        System.out.println("Digite el nombre de la obra : ");
        String nombreObra = in.readLine();
        System.out.println("Digite la fecha de ingreso del libro");
        String fechaIngreso = in.readLine();
        System.out.println("Digite la cantidad de paginas que posee el libro:");
        int cantidadPaginas = Integer.parseInt(in.readLine());
        System.out.println("Datos del autor:");
        System.out.println("Digite el nombre  del autor : ");
        String nombre = in.readLine();
        System.out.println("Digite el pais de nacimiento del autor : ");
        String pais = in.readLine();
        System.out.println("Digite la provincia de nacimiento del autor: ");
        String lugarNacimiento = in.readLine();
        System.out.println("Digite la nacionalidad del autor: ");
        String nacionalidad = in.readLine();
        System.out.println("Digite el año de nacimiento del autor :");
        int fechaNacimiento = Integer.parseInt(in.readLine());
        int edad = calculoEdad(fechaNacimiento, fechaActual);
        System.out.println("Edad del Autor : " + edad);
        Autor datosAutor = new Autor(nombre, pais, lugarNacimiento, nacionalidad, fechaNacimiento, edad);
        Libro libros = new Libro(fechaPublicacion, nombreObra, fechaIngreso, cantidadPaginas, ISBN, datosAutor);
        validacionLibros(ISBN, libros, nombre);
        agregarAutor(datosAutor);
    }// fin

    public static void validacionLibros(int ISBN, Libro libros, String nombre) {

        for (int i = 0; i < listaLibros.length; i++) {

            if (listaLibros[i] != null) {
                if (ISBN == listaLibros[i].getISBN()) {
                    i = listaLibros.length;
                    System.out.println("Libro con codigo ya existente");
                }
            } else {
                listaLibros[i] = libros;
                System.out.println("Libro agregado correctamente");
                i = listaLibros.length;
            }
        }
    }//fin 

    public static void agregarAutor(Autor datosAutor) {
        for (int i = 0; i < listaAutores.length; i++) {
            if (listaAutores[i] == null) {
                listaAutores[i] = datosAutor;
                System.out.println("Se agrego un autor a la lista");
                i = listaLibros.length;
            } else {
                i = listaLibros.length;
            }
        }
    }//fin

    public static int calculoEdad(int fechaNacimiento, int fechaActual) {
        int edad = fechaActual - fechaNacimiento;
        return edad;
    }//fin

    public static void registroCategorias() throws IOException {
        System.out.println("Por favor ingrese el nombre de la categoria");
        String nombreCategoria = in.readLine();
        int codigo = generarCodigo();

        Categorias categ = new Categorias(nombreCategoria, codigo);

        for (int i = 0; i < listaCategorias.length; i++) {
            if (listaCategorias[i] == null) {
                listaCategorias[i] = categ;
                System.out.println("El codigo de la categoria " + " " + nombreCategoria + " " + "es: " + " " + codigo);
                i = listaCategorias.length;
            } else if (listaCategorias[i] != null) {
                if (categ.equals(listaCategorias[i]) || codigo == listaCategorias[i].getCodigo() || nombreCategoria.equals(listaCategorias[i].getNombreCategoria())) {
                    i = listaCategorias.length;
                    System.out.println("Nombre de categoria ya existente.");
                }
            }
        }
    }//fin

    public static void registroClientes() throws IOException {
        System.out.println("Digite el nombre completo del cliente  :");
        String nombreCliente = in.readLine();
        System.out.println("Digite la identificacion del cliente  :");
        String identificacion = in.readLine();
        System.out.println("Digite el pais de nacimiento del cliente : ");
        String paisCliente = in.readLine();
        Clientes cliente = new Clientes(nombreCliente, identificacion, paisCliente);
        validarCliente(identificacion, cliente);
    }

    public static void validarCliente(String identificacion, Clientes cliente) {
        for (int i = 0; i < listaClientes.length; i++) {
            if (listaClientes[i] != null) {
                if (identificacion.equals(listaClientes[i].getId())) {
                    System.out.println("Cliente ya registrado");
                    i = listaClientes.length;
                }
            } else {
                listaClientes[i] = cliente;
                i = listaClientes.length;
                System.out.println("Cliente registrado correctamente");
                i = listaClientes.length;
            }
        }
    }//fin

    public static void listarLibros() {
        for (int i = 0; i < listaLibros.length; i++) {
            if (listaLibros[i] != null) {
                System.out.println(listaLibros[i].toString());
            }
        }
    }//fin

    public static void listarAutor() {
        for (int i = 0; i < listaLibros.length; i++) {
            if (listaAutores[i] != null) {
                System.out.print(listaAutores[i].toString() + "\n");
            }
        }
    }//fin

    public static void listarCategorias() {
        for (int i = 0; i < listaCategorias.length; i++) {
            if (listaCategorias[i] != null) {
                System.out.println(listaCategorias[i].toString());
            }
        }
    }//fin

    public static void listarClientes() {
        for (int i = 0; i < listaLibros.length; i++) {
            if (listaClientes[i] != null) {
                System.out.println(listaClientes[i].toString());
            }
        }
    }// fin

    public static int generarCodigo() {
        int codigo = (int) (Math.random() * 5 + 1);
        for (int i = 0; i < listaCodigos.length; i++) {
            if (listaCategorias[i] != null) {
                if (codigo == listaCodigos[i]) {
                    System.out.println("El código ya existe");
                    i = listaCodigos.length;
                }
            } else {
                listaCodigos[i] = codigo;
                i = listaCodigos.length;
                System.out.println("El código fue agregado correctamente");
            }
        }//fin
        return codigo;
    }

}//fin
