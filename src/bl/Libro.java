package bl;


public class Libro {
    private int fechaPublicacion;
    private String nombreObra;
    private String fechaIngreso;
    private int cantidadPaginas;
    private int ISBN;
    private Autor autor;

    public Libro() {
        this.fechaPublicacion = 0;
        this.nombreObra = "";
        this.fechaIngreso = "";
        this.cantidadPaginas = 0;
        this.ISBN = 0;
        this.autor = null;
    }

    public Libro(int fechaPublicacion, String nombreObra, String fechaIngreso, int cantidadPaginas, int ISBN, Autor autor) {
        this.fechaPublicacion = fechaPublicacion;
        this.nombreObra = nombreObra;
        this.fechaIngreso = fechaIngreso;
        this.cantidadPaginas = cantidadPaginas;
        this.ISBN = ISBN;
        this.autor = autor;
    }

    public int getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(int fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getNombreObra() {
        return nombreObra;
    }

    public void setNombreObra(String nombreObra) {
        this.nombreObra = nombreObra;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getCantidadPaginas() {
        return cantidadPaginas;
    }

    public void setCantidadPaginas(int cantidadPaginas) {
        this.cantidadPaginas = cantidadPaginas;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    @Override
    public String toString() {
        return "Libro{" + "fechaPublicacion=" + fechaPublicacion 
                + ", nombreObra=" + nombreObra + ", fechaIngreso=" 
                + fechaIngreso + ", cantidadPaginas=" + cantidadPaginas 
                + ", ISBN=" + ISBN + ", autor=" + autor + '}';
    }
 
    
    
}//fin
