package bl;

public class Categorias {

    private String nombreCategoria;
    private int codigo;

    public Categorias() {
        this.nombreCategoria = "";
        this.codigo = 0;
    }

    public Categorias(String nombreCategoria, int codigo) {
        this.nombreCategoria = nombreCategoria;
        this.codigo = codigo;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String toString() {
        return "Catergorias{" + "nombreCategoria="
                + nombreCategoria + ", codigo=" + codigo + '}';
    }

    
}//fin
